<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use dektrium\user\helpers\Timezone;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $model
 */

$this->title = Yii::t('user', 'ตั้งค่าข้อมูลส่วนตัว');
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="row">
    <div class="col-md-3">
        <?= $this->render('_menu') ?>
    </div>
    <div class="col-md-9">
        <div style="font-size: 15px;" class="pl-sm-5 pr-sm-5">
            <h4 class="pt-10" style="font-weight: bold;"><?= Html::encode($this->title) ?></h4>
            <br>
            <div class="filters-content">
                <?php $form = ActiveForm::begin([
                    'id' => 'profile-form',
                    'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnBlur' => false,
                ]); ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => true]) ?>

                <?= $form->field($model, 'departmentname')->textInput(['maxlength' => true, 'disabled' => true]) ?>

                <?= $form->field($model, 'tel')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'public_email')->textInput(['maxlength' => true]) ?>

                <div class="container">
                    <div class="row">
                        <div class="col-xs-10 col-md-10 col-lg-10">
                            <div class="well text-center">
                                <?= Html::img($model->getPhotoViewer(), ['style' => 'width:100px;', 'class' => 'img-rounded']); ?>
                            </div>
                        </div>
                        <div class="col-xs-10 col-md-10 col-lg-10">
                            <?= $form->field($model, 'photo')->fileInput()->label(false) ?>
                        </div>
                    </div>
                </div>

                <div class="row home-about-right pl-90">
                    <div class="col-12">
                        <div class="form-group">
                            <?= Html::submitButton('บันทึก', ['class' => 'primary-btn text-uppercase float-right']) ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>

        </div>
    </div>
</div>


<script>
    path = '?r=user/settings/profile';
    subpath = path;

    var target = $('nav a[href="' + path + '"]');
    // Add active class to target link
    target.addClass('menu-active');


</script>
