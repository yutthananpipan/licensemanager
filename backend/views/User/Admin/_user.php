<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var yii\bootstrap4\ActiveForm $form
 * @var dektrium\user\models\User $user
 */

use yii\helpers\Html; ?>

<?= $form->field($user, 'email')->textInput(['maxlength' => 255]) ?>
<?= $form->field($user, 'username')->textInput(['maxlength' => 255]) ?>
<?= $form->field($user, 'password')->passwordInput() ?>
<?= $form->field($user, 'roles')->checkboxList($user->getAllRoles()) ?>

<div>
    <span style="color: #777777; font-weight: bold;">รายละเอียด <?= $user->getAttributeLabel('roles') ?></span>
    <div>
        <?php
        $user_roles = $user->getAllRolesDesc();
        foreach ($user_roles as $role) {
            ?>
            <div></div>
            <div></div>
            <?php
            echo Html::tag('span', Html::encode($role['name']), ['class' => 'username', 'style'=> 'color:#33C0A4;']);
            echo Html::tag('p', Html::encode($role['description']), ['class' => 'username', ]);
        }
        ?>
    </div>
</div>