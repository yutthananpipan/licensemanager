<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\User $user
 * @var dektrium\user\models\Profile $profile
 */
?>

<?php $this->beginContent('@dektrium/user/views/admin/update.php', ['user' => $user]) ?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'options' => ['enctype' => 'multipart/form-data'],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-9',
        ],
    ],
]); ?>

<?= $form->field($profile, 'name')->textInput(['maxlength' => true, 'disabled' => false]) ?>

<?= $form->field($profile, 'department_id')->dropDownList($profile->getDepartments()) ?>

<?= $form->field($profile, 'tel')->textInput(['maxlength' => true]) ?>

<?= $form->field($profile, 'public_email')->textInput(['maxlength' => true]) ?>

<div class="container">
    <div class="row">
        <div class="col-xs-10 col-md-10 col-lg-10">
            <div class="well text-center">
                <?= Html::img($profile->getPhotoViewer(), ['style' => 'width:100px;', 'class' => 'img-rounded']); ?>
            </div>
        </div>
        <div class="col-xs-10 col-md-10 col-lg-10">
            <?= $form->field($profile, 'photo')->fileInput() ?>
        </div>
    </div>
</div>

<div class="row home-about-right pl-90">
    <div class="col-12">
        <div class="form-group mr-auto mr-lg-5">
            <?= Html::submitButton('แก้ไข', ['class' => 'primary-btn text-uppercase float-right']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $this->endContent() ?>
