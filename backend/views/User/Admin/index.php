<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;


/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var common\models\UserSearch $searchModel
 */

$this->title = Yii::t('user', 'จัดการสมาชิก');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_mainmenu') ?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<?= $this->render('_menu') ?>

<div>
    <?php Pjax::begin() ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['class' => 'table-hover table-responsive'],
        'headerRowOptions' => ['class' => 'text-center'],
        'layout' => "{items}\n{pager}",
        'pager' => [
            'options' => [
                'class' => 'pagination',
                //'style' => ['margin-left' => '15px'],
            ],

            // Customzing CSS class for pager link
            'linkOptions' => ['class' => 'page-link'],
            'activePageCssClass' => 'active',
            'disabledPageCssClass' => 'disable',

            // Customzing CSS class for navigating link
            'prevPageCssClass' => 'mypre',
            'nextPageCssClass' => 'mynext',
            'firstPageCssClass' => 'myfirst',
            'lastPageCssClass' => 'mylast',

            //'firstPageLabel' => 'first',
            //'lastPageLabel' => 'last',
            //'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
            //'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
        ],
        'columns' => [
            [
                'attribute' => 'id',
                'value' => 'id'
            ],
            [
                'attribute' => 'roles',
                'headerOptions' => ['style' => 'width:140px;'],
                'value' => function ($data) {
                    $roles = \Yii::$app->authManager->getRolesByUser($data->id);
                    if ($roles) {
                        return implode(', ', array_keys($roles));
                    } else {
                        return '';
                    }
                }

            ],
            [
                'headerOptions' => ['style' => 'width:120px;'],
                'attribute' => 'username',
                'value' => 'username',

            ],
            [
                'headerOptions' => ['style' => 'width:140px;'],
                'attribute' => 'profile.name',
                'value' => function ($model) {
                    if($model->profile->name != null){
                        return  $model->profile->name;
                    }else{
                        return  '';
                    }
                },
            ],
            [
                'headerOptions' => ['style' => 'width:140px;'],
                'attribute' => 'profile.department',
                'value' => function ($model) {
                    if($model->profile->departmentname != null){
                        return  $model->profile->departmentname;
                    }else{
                        return  '';
                    }
                },
            ],
            [
                'attribute' => 'email',
                'value' => 'email',

            ],
            [
                'attribute' => 'last_login_at',
                'value' => function ($model) {
                    if (!$model->last_login_at || $model->last_login_at == 0) {
                        return Yii::t('user', 'Never');
                    } else if (extension_loaded('intl')) {
                        return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->last_login_at]);
                    } else {
                        return date('Y-m-d G:i:s', $model->last_login_at);
                    }
                },
            ],
            [
                'header' => Yii::t('user', 'Confirmation'),
                'value' => function ($model) {
                    if ($model->isConfirmed) {
                        return '<div class="text-center">
                                <span class="text-success">' . Yii::t('user', 'Confirmed') . '</span>
                            </div>';
                    } else {
                        return Html::a(Yii::t('user', 'Confirm'), ['confirm', 'id' => $model->id], [
                            'class' => 'btn btn-xs btn-success btn-block',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                        ]);
                    }
                },
                'format' => 'raw',
                'visible' => Yii::$app->getModule('user')->enableConfirmation,
            ],
            [
                'header' => Yii::t('user', 'Block status'),
                'value' => function ($model) {
                    if ($model->isBlocked) {
                        return Html::a(Yii::t('user', 'Unblock'), ['block', 'id' => $model->id], [
                            'class' => 'btn btn-xs btn-success btn-block',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                        ]);
                    } else {
                        return Html::a(Yii::t('user', 'Block'), ['block', 'id' => $model->id], [
                            'class' => 'btn btn-xs btn-danger btn-block',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                        ]);
                    }
                },
                'format' => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'จัดการ',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'template' => '<div class="btn-group btn-group-sm text-center" role="group">{update} {resend_password}{delete}</div>',
                'options' => ['style' => 'width:80px;'],
                'buttons' => [
                    'resend_password' => function ($url, $model, $key) {
                        if (\Yii::$app->user->identity->isAdmin && !$model->isAdmin) {
                            return '
                    <a class = "btn btn-sm btn-outline-secondary text-info" data-method="POST" data-confirm="' . Yii::t('user', 'Are you sure?') . '" href="' . Url::to(['resend-password', 'id' => $model->id]) . '">
                    <span title="' . Yii::t('user', 'Generate and send new password to user') . '" > <i class="fa fa-envelope"></i>
                    </span> </a>';
                        }
                    },
                    'switch' => function ($url, $model) {
                        if (\Yii::$app->user->identity->isAdmin && $model->id != Yii::$app->user->id && Yii::$app->getModule('user')->enableImpersonateUser) {
                            return Html::a('<i class="fa fa-user"></i>', ['/user/admin/switch', 'id' => $model->id], [
                                'class' => 'btn btn-sm btn-outline-secondary text-info',
                                'title' => Yii::t('user', 'Become this user'),
                                'data-confirm' => Yii::t('user', 'Are you sure you want to switch to this user for the rest of this Session?'),
                                'data-method' => 'POST',
                            ]);
                        }
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('<i class="	fas fa-edit"></i>', $url,
                            [
                                'class' => 'btn btn-sm btn-outline-secondary text-info',
                                'title' => Yii::t('user', 'แก้ไขข้อมูลผู้ใช้งาน'),
                            ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-trash"></i>', $url, [
                            'class' => 'btn btn-sm btn-outline-secondary text-danger',
                            'title' => Yii::t('user', 'ลบข้อมูลผู้ใช้งาน'),
                            'data-confirm' => Yii::t('user', 'คุณต้องการลบข้อมูลนี้หรือไม่'),
                            'data-method' => 'POST',
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end() ?>
</div>

<div class="row home-about-right pl-90">
    <div class="col-12">
        <div class="form-group">
            <?= Html::a('เพิ่มผุู้ใช้งาน', ['create'], ['class' => 'primary-btn text-uppercase float-right']) ?>
        </div>
    </div>
</div>


<script>
    path = '?r=user/admin';
    subpath = path;

    var target = $('nav a[href="' + path + '"]');
    // Add active class to target link
    target.addClass('menu-active');

    var sub_taget = $('div a[href="' + subpath + '"]');
    sub_taget.addClass('menu-active');

</script>