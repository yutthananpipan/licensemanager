<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%project}}".
 *
 * @property string $number
 * @property string $name
 * @property string $version
 *
 * @property License[] $licenses
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%project}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number'], 'required'],
            [['version'], 'string', 'max' => 11],
            [['number'], 'string', 'max' => 11],
            [['name'], 'string', 'max' => 255],
            [['number'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'number' => 'Number',
            'name' => 'Name',
            'version' => 'Version',
        ];
    }

    /**
     * Gets query for [[Licenses]].
     *
     * @return \yii\db\ActiveQuery|LicenseQuery
     */
    public function getLicenses()
    {
        return $this->hasMany(License::className(), ['project_number' => 'number']);
    }

    /**
     * {@inheritdoc}
     * @return ProjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProjectQuery(get_called_class());
    }
}
