<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%license}}".
 *
 * @property int $id
 * @property string|null $auth_key
 * @property int|null $license_user
 * @property int|null $confirmed_at
 * @property int|null $blocked_at
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $expire_date
 * @property string|null $cust_number
 * @property string|null $project_number
 *
 * @property Customer $custNumber
 * @property Project $projectNumber
 */
class License extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%license}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['license_user', 'confirmed_at', 'blocked_at', 'created_at', 'updated_at', 'created_by', 'updated_by', 'expire_date'], 'integer'],
            [['auth_key'], 'string', 'max' => 255],
            [['cust_number', 'project_number'], 'string', 'max' => 11],
            [['cust_number'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['cust_number' => 'id']],
            [['project_number'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_number' => 'number']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auth_key' => 'Auth Key',
            'license_user' => 'License User',
            'confirmed_at' => 'Confirmed At',
            'blocked_at' => 'Blocked At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'expire_date' => 'Expire Date',
            'cust_number' => 'Cust Number',
            'project_number' => 'Project Number',
        ];
    }

    /**
     * Gets query for [[CustNumber]].
     *
     * @return \yii\db\ActiveQuery|CustomerQuery
     */
    public function getCustNumber()
    {
        return $this->hasOne(Customer::className(), ['id' => 'cust_number']);
    }

    /**
     * Gets query for [[ProjectNumber]].
     *
     * @return \yii\db\ActiveQuery|ProjectQuery
     */
    public function getProjectNumber()
    {
        return $this->hasOne(Project::className(), ['number' => 'project_number']);
    }

    /**
     * {@inheritdoc}
     * @return LicenseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LicenseQuery(get_called_class());
    }
}
