<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[License]].
 *
 * @see License
 */
class LicenseQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return License[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return License|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
