<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'timeZone' => 'Asia/Bangkok',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'authManager' => [
            // 'class' =>  'yii\rbac\PhpManager',
            'class' => 'yii\rbac\DbManager',
        ],
        'formatter' => [
            'dateFormat' => 'yyyy-mm-dd 00:00:00',
            'datetimeFormat' => 'yyyy-MM-dd H:i:s',
            'timeFormat' => 'H:i:s',

            'locale' => 'th-TH', //your language locale
            'defaultTimeZone' => 'Asia/Bangkok', // time zone
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];
